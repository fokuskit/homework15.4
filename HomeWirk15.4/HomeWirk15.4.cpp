#include <iostream>


void function(int i, int n)
{

	for (int b = i; b <= n; b++)
	{
		std::cout << b << "\n";
		b++;
	}
}

int main()
{
	int i, n;
	std::cout << "Input parity: ";
	std::cin >> i;
	std::cout << "Input until which date: ";
	std::cin >> n;

	function(i, n);
}